/**
 * Created by jake on 6/25/14.
 */
import java.util.Random;
public class Tree {

	double circumference = 0.0;
	int serial = 0;
	String species = "error";

	Tree (int serial, double circumference, String species){
		this.circumference = circumference;
		this.serial = serial;
		this.species = species;
	}

	public static void main(String[] args){
//		Random rnd = new Random();
		TreeTracker tt = new TreeTracker(43);
		tt.printTrees();
	}

	public String describe (){
		return String.format("Tree number %d has a circumference of %.2f and is of species %s.", serial, circumference, species);
	}
}

class TreeTracker {


	Tree[] trees;
	TreeTracker(int quantity){
		Random rnd = new Random();
		trees = new Tree[quantity];
		for (int i = 0; i < quantity; i++) {
			trees[i] = new Tree(i, rnd.nextDouble() * 10, "s" + rnd.nextInt(13));
		}
	}


	void printTrees (){
		String printable = "";
		for (Tree  i : trees){
			printable += i.describe() + "\n";
		}
		System.out.printf(printable);
	}
}

